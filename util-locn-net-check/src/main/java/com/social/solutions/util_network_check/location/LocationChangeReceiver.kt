package com.social.solutions.util_network_check.location

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class LocationChangeReceiver: BroadcastReceiver() {

    companion object{
        fun create(callbacks: List<OnLocationStateCallback>): BroadcastReceiver {
            val receiver = LocationChangeReceiver()
            receiver.mCallbacks = callbacks
            return receiver
        }
    }

    var mCallbacks: List<OnLocationStateCallback>? = null

    override fun onReceive(context: Context?, intent: Intent?) {
        context ?: return
        emmitLocationStateChanged(LocationUtil.isEnabled(context))
    }

    private fun emmitLocationStateChanged(isEnabled: Boolean) {
        val callbacks = mCallbacks ?: return
        for (callback in callbacks) {
            if(isEnabled)
                callback.onLocationEnabled()
            else
                callback.onLocationDisabled()
        }
    }
}