package com.social.solutions.util_network_check.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class NetworkChangeReceiver: BroadcastReceiver() {

    companion object{
        fun create(callbacks: List<OnNetworkStateCallback>): BroadcastReceiver {
            val receiver = NetworkChangeReceiver()
            receiver.mCallbacks = callbacks
            return receiver
        }
    }

    var mCallbacks: List<OnNetworkStateCallback>? = null

    override fun onReceive(context: Context?, intent: Intent?) {
        context ?: return
        emmitNetworkStateChanged(NetworkUtil.isConnected(context))
    }

    private fun emmitNetworkStateChanged(isConnected: Boolean) {
        val callbacks = mCallbacks ?: return
        for (callback in callbacks) {
            if(isConnected)
                callback.onInternetConnected()
            else
                callback.onInternetDisconnected()
        }
    }
}