package com.social.solutions.util_network_check.location

interface OnLocationStateCallback {
    fun onLocationEnabled()
    fun onLocationDisabled()
}
