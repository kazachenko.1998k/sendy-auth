package com.social.solutions.util_network_check.location

import android.content.Context
import android.location.LocationManager

object LocationUtil {
    fun isEnabled(context: Context): Boolean {
        val lm: LocationManager =
            context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gps_enabled = false
        var network_enabled = false

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (ex: Exception) {ex.printStackTrace()}

        try {
            network_enabled =
                lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (ex: Exception) {ex.printStackTrace()}

        return gps_enabled && network_enabled
    }
}