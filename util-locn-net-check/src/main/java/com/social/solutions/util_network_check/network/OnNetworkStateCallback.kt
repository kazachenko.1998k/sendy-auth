package com.social.solutions.util_network_check.network

interface OnNetworkStateCallback {
    fun onInternetConnected()
    fun onInternetDisconnected()
}
