package com.social.solution.lib_auth

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_auth_api.AuthorizationFeatureApi
import com.social.solution.feature_auth_impl.AuthorizationFeatureImpl
import com.social.solution.feature_auth_impl.start.AuthorizationStarterImpl

object LibAuthDependency {

    fun featureAuthApi(): AuthorizationFeatureApi {
        return AuthorizationFeatureImpl(AuthorizationStarterImpl())
    }
}