package com.social.solution.feature_auth_api

import androidx.lifecycle.MutableLiveData

interface AuthorizationFeatureApi {
    fun authorizationStarter(): AuthorizationStarter
    fun isAuthorized(): MutableLiveData<Boolean>
}