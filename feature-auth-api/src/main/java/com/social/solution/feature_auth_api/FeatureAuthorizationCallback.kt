package com.social.solution.feature_auth_api

interface FeatureAuthorizationCallback {
    fun onSignedAnonym(userUID: Long)
    fun onAuthorized(userUID: Long, isNeedNick: Boolean)
    fun onExit()
}