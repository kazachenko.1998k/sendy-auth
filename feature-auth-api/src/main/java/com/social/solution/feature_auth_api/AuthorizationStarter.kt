package com.social.solution.feature_auth_api

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.morozov.core_backend_api.FeatureBackendApi

interface AuthorizationStarter {

    fun start(
        manager: FragmentManager, container: Int, addToBackStack: Boolean,
        callback: FeatureAuthorizationCallback,
        networkState: LiveData<Boolean>,
        backendApi: FeatureBackendApi
    )

    fun start(
        manager: FragmentManager, container: Int, addToBackStack: Boolean,
        callback: FeatureAuthorizationCallback,
        networkState: LiveData<Boolean>,
        backendApi: LiveData<FeatureBackendApi?>
    )
}
