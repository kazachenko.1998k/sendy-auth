package com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.models

class CountryItemModel(val firstLetter: Char, val countryModel: CountryModel?)