package com.social.solution.feature_auth_impl.repository

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.morozov.core_backend_api.AnyInnerRequest
import com.morozov.core_backend_api.AnyRequest
import com.morozov.core_backend_api.auth.AuthApi
import com.morozov.core_backend_api.auth.modelsRequest.AuthCheckCodeRequest
import com.morozov.core_backend_api.auth.modelsRequest.AuthSignAnonymRequest
import com.morozov.core_backend_api.auth.modelsRequest.AuthSignPhoneRequest
import com.morozov.core_backend_api.auth.modelsResponse.AuthCheckCodeResponse
import com.morozov.core_backend_api.auth.modelsResponse.AuthSignAnonymResponse
import com.morozov.core_backend_api.auth.modelsResponse.AuthSignPhoneResponse
import com.social.solution.feature_auth_impl.ui.MainObject

fun TextView.setSignAnonymButton(
    activity: Activity?, lifecycleOwner: LifecycleOwner,
    sign: String, authApi: AuthApi,
    callback: (response: AuthSignAnonymResponse) -> Unit = {}
) {
    setOnClickListener {
        AuthRepository.signAnonym(activity, sign, authApi).observe(lifecycleOwner, Observer {
            callback(it)
        })
    }
}

fun View.setSignPhoneButton(
    activity: Activity?, lifecycleOwner: LifecycleOwner, sign: String,
    authApi: AuthApi, getPhoneNumber: () -> String,
    callback: (response: AuthSignPhoneResponse) -> Unit = {}
) {
    setOnClickListener {
        AuthRepository.signPhone(activity, authApi, getPhoneNumber())
            .observe(lifecycleOwner, Observer {
                callback(it)
            })
    }
}

fun Button.setCheckCodeButton(
    lifecycleOwner: LifecycleOwner,
    getCode: () -> Int,
    callback: (response: AuthCheckCodeResponse) -> Unit = {}
) {
    setOnClickListener {
        AuthRepository.checkCode(getCode()).observe(lifecycleOwner, Observer {
            callback(it)
        })
    }
}

object AuthRepository {

    @SuppressLint("HardwareIds")
    internal fun signPhone(
        activity: Activity?,
        authApi: AuthApi,
        phoneNumber: String
    ): LiveData<AuthSignPhoneResponse> {
        val responce = MutableLiveData<AuthSignPhoneResponse>()
        val deviceInfo = Settings.Secure.getString(
            activity?.applicationContext?.contentResolver,
            Settings.Secure.ANDROID_ID
        )
        val deviceModel = Build.BRAND + "_" + Build.MODEL
        val notificationToken = activity?.getSharedPreferences("AUTH", Context.MODE_PRIVATE)
            ?.getString("TOKEN_FIREBASE", null) ?: ""
        val param = AuthSignPhoneRequest(
            deviceInfo,
            phoneNumber.toLongOrNull(),
            deviceModel,
            Build.VERSION.SDK_INT,
            notificationToken
        )
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)
        authApi.signPhone(data) { event ->
            responce.postValue(event)
        }
        return responce
    }

    @SuppressLint("HardwareIds")
    internal fun signAnonym(
        activity: Activity?,
        sign: String,
        authApi: AuthApi
    ): LiveData<AuthSignAnonymResponse> {
        val responce = MutableLiveData<AuthSignAnonymResponse>()
        val deviceInfo = Settings.Secure.getString(
            activity?.applicationContext?.contentResolver,
            Settings.Secure.ANDROID_ID
        )
        val deviceModel = Build.BRAND + "_" + Build.MODEL
        val notificationToken = activity?.getSharedPreferences("AUTH", Context.MODE_PRIVATE)
            ?.getString("TOKEN_FIREBASE", null) ?: ""
        val param =
            AuthSignAnonymRequest(deviceInfo, deviceModel, Build.VERSION.SDK_INT, notificationToken)
        val sendMessage = AnyInnerRequest(param = param)
        val data = AnyRequest(sendMessage)
        authApi.signAnonim(data) { event ->
            responce.postValue(event)
        }
        return responce
    }

    @SuppressLint("HardwareIds")
    internal fun checkCode(code: Int): LiveData<AuthCheckCodeResponse> {
        val response = MutableLiveData<AuthCheckCodeResponse>()
        val param = AuthCheckCodeRequest(code)
        val sendMessage = AnyInnerRequest(param)
        val data = AnyRequest(sendMessage)
        MainObject.mBackendApi?.let {
            it.authApi().checkCode(data) {
                Log.e("WebSocket", it.toString())
                response.postValue(it)
            }
        }
        return response
    }
}