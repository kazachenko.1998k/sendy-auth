package com.social.solution.feature_auth_impl.start

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.NavHostFragment
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_auth_api.AuthorizationStarter
import com.social.solution.feature_auth_api.FeatureAuthorizationCallback
import com.social.solution.feature_auth_impl.R
import com.social.solution.feature_auth_impl.ui.MainObject

class AuthorizationStarterImpl : AuthorizationStarter {

    override fun start(manager: FragmentManager,
                       container: Int,
                       addToBackStack: Boolean,
                       callback: FeatureAuthorizationCallback,
                       networkState: LiveData<Boolean>,
                       backendApi: FeatureBackendApi) {
        MainObject.mCallback = callback
        MainObject.mBackendApi = backendApi
        MainObject.mNetworkState = networkState
        val finalHost = NavHostFragment.create(R.navigation.nav_host)
        manager.beginTransaction()
            .replace(container, finalHost)
            .setPrimaryNavigationFragment(finalHost)
            .commitAllowingStateLoss()
    }

    override fun start(manager: FragmentManager,
                       container: Int,
                       addToBackStack: Boolean,
                       callback: FeatureAuthorizationCallback,
                       networkState: LiveData<Boolean>,
                       backendApi: LiveData<FeatureBackendApi?>) {
        MainObject.mCallback = callback
        MainObject.mBackendApiLiveData = backendApi
        MainObject.mNetworkState = networkState
        val finalHost = NavHostFragment.create(R.navigation.nav_host)
        manager.beginTransaction()
            .replace(container, finalHost)
            .setPrimaryNavigationFragment(finalHost)
            .commitAllowingStateLoss()
    }
}