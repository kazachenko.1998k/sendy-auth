package com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.models.CountryItemModel
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.models.CountryModel
import kotlinx.android.synthetic.main.item_header_letter.view.*

class HeaderLetterViewHolder(itemView: View): BaseCountryViewHolder(itemView) {
    override fun populate(countryItemModel: CountryItemModel, listener: View.OnClickListener) {
        itemView.textLetter.text = countryItemModel.firstLetter.toString()
    }
}