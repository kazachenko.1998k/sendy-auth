package com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.holders

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.models.CountryItemModel
import kotlinx.android.synthetic.main.item_header_letter.view.*
import kotlinx.android.synthetic.main.item_search.view.*

class SearchViewHolder(itemView: View): BaseCountryViewHolder(itemView) {
    override fun populate(countryItemModel: CountryItemModel, listener: View.OnClickListener) {}

    fun populate(textWatcher: TextWatcher) {
        itemView.editSearchCountry.addTextChangedListener(textWatcher)
        itemView.editSearchCountry.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(string: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (string!=null)
                    if (string.isNotEmpty()) {
                        itemView.imageClear.visibility = View.VISIBLE
                        return
                    }
                itemView.imageClear.visibility = View.GONE
            }
        })

        itemView.imageClear.setOnClickListener {
            itemView.editSearchCountry.text.clear()
        }
    }
}