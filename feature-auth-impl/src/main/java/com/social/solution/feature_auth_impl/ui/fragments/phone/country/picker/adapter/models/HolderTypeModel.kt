package com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.models

enum class HolderTypeModel(val value: Int) {
    SEARCH(0),
    HEADER(1),
    COUNTRY(2)
}