package com.social.solution.feature_auth_impl.utils

import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.widget.TextView
import com.social.solution.feature_auth_impl.R

fun TextView.highlightPartAccent(start: Int, end: Int) {
    val spannable = SpannableString(this.text)

    spannable.setSpan(
        ForegroundColorSpan(resources.getColor(R.color.colorAccent)),
        start,
        end,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )

    setText(spannable, TextView.BufferType.SPANNABLE)
}

fun TextView.highlightPartAccent(string: String) {
    val spannable = SpannableString(this.text)

    val start = text.toString().split(string).first().length
    val end = start + string.length

    Log.i("Jeka", "Start: $start, end: $end")

    spannable.setSpan(
        ForegroundColorSpan(resources.getColor(R.color.colorAccent)),
        start,
        end,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )

    setText(spannable, TextView.BufferType.SPANNABLE)
}

fun TextView.highlightPartAccent(string: String, click: () -> Unit) {
    val spannable = SpannableString(this.text)

    val start = text.toString().split(string).first().length
    val end = start + string.length

    Log.i("Jeka", "Start: $start, end: $end")

    val clickSpan = object : ClickableSpan() {
        override fun onClick(widget: View) {
            click()
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }

    movementMethod = LinkMovementMethod.getInstance()

    spannable.setSpan(
        clickSpan,
        start,
        end,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )

    setText(spannable, TextView.BufferType.SPANNABLE)
}