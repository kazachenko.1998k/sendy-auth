package com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.sticky.header

import android.view.View
import com.social.solution.feature_auth_impl.R
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.CountryAdapter
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.models.HolderTypeModel
import kotlinx.android.synthetic.main.item_header_letter.view.*
import kotlinx.android.synthetic.main.item_header_letter_stick.view.*

class StickyCountryAdapter: CountryAdapter(), StickyHeaderInterface {
    override fun getHeaderPositionForItem(itemPositionCnst: Int): Int {
        var itemPosition = itemPositionCnst
        var headerPosition = 0
        do {
            if (isHeader(itemPosition)) {
                headerPosition = itemPosition
                break
            }
            itemPosition -= 1
        } while (itemPosition >= 0)
        return headerPosition
    }

    override fun getHeaderLayout(headerPosition: Int): Int = R.layout.item_header_letter_stick

    override fun bindHeaderData(header: View?, headerPosition: Int) {
        header?:return
        header.textLetterStick.text = data()[headerPosition].firstLetter.toString()
    }

    override fun isHeader(itemPosition: Int): Boolean {
        return data()[itemPosition].countryModel == null
    }
}