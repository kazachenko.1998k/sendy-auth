package com.social.solution.feature_auth_impl.ui.fragments.sms

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.social.solution.feature_auth_impl.R
import com.social.solution.feature_auth_impl.repository.AuthRepository
import com.social.solution.feature_auth_impl.repository.parseSignPhone
import com.social.solution.feature_auth_impl.ui.MainObject
import com.social.solution.feature_auth_impl.utils.formatMillis
import com.social.solution.feature_auth_impl.utils.hideKeyboard
import com.social.solution.feature_auth_impl.utils.showKeyboard
import kotlinx.android.synthetic.main.fragment_sms_code.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*

class SmsCodeFragment : Fragment() {
    companion object {
        const val TAG = "SmsCodeFragment_TAG"
        private const val trueCode = "1234"

        var timeToCode = 240
    }

    private var userCode = ""
    private var currentNumber = 0
    private var beforeText = ""
    private var afterText = ""

    private val timerSeconds = MutableLiveData<Int>()

    private lateinit var mPhoneNumber: String

    private var isNetworkEnable = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_sms_code, container, false)

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        MainObject.mBackendApiLiveData?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it != null) {
                initSendCodeAgain()
                MainObject.mBackendApi = it
            }
        })

        showKeyboard(context)

        linearBack.setOnClickListener {
            activity?.onBackPressed()
        }

        val locale = context?.resources?.configuration?.locale?.country
        println("Jeka: " + locale)

        mPhoneNumber = arguments?.getString("phoneNumber") ?: "79531405321"

        val styledText = "Сообщение отправлено на номер <font color='black'>+$mPhoneNumber</font>"
        textViewWithPhone.setText(
            Html.fromHtml(styledText),
            TextView.BufferType.SPANNABLE
        )

        editNumbers.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s ?: return
                beforeText = afterText
                afterText = s.toString()
                if (beforeText.length < afterText.length && currentNumber < 5) {
                    showEditNumber(currentNumber, afterText[afterText.length - 1].toString())
                    currentNumber++
                }
                if (beforeText.length > afterText.length && currentNumber > 0) {
                    currentNumber--
                    hideEditNumber(currentNumber)
                }
            }
        })

        timerSeconds.observe(this, Observer<Int> { seconds ->
            if (seconds == null) {
                linearNewCode.visibility = View.VISIBLE
                linearWait.visibility = View.INVISIBLE
            } else {
                textSeconds.text = (seconds * 1000L).formatMillis()
            }
        })

        getNewCode()

        if (MainObject.mBackendApi != null)
            initSendCodeAgain()

        MainObject.mNetworkState?.observe(this, androidx.lifecycle.Observer {
            isNetworkEnable = it
            if (isNetworkEnable.not())
                showNoInternet()
        })
    }

    private fun initSendCodeAgain() {
        linearNewCode.setOnClickListener {
            if (isNetworkEnable.not()) {
                showNoInternet()
                return@setOnClickListener
            }
            MainObject.mBackendApi?.let { it1 ->
                AuthRepository.signPhone(activity, it1.authApi(), getPhoneNumber())
                    .observe(this, Observer { response ->
                        if (response.result.not()) {
                            if (response.error != null)
                                Toast.makeText(
                                    context,
                                    response.error!!.parseSignPhone(resources),
                                    Toast.LENGTH_SHORT
                                ).show()
                            else
                                Toast.makeText(
                                    context,
                                    resources.getString(R.string.error_response),
                                    Toast.LENGTH_SHORT
                                ).show()
                        } else {
                            timeToCode =
                                ((response.data!!.nextCodeTime * 1000 - Calendar.getInstance().timeInMillis) / 1000 + 1).toInt()
                            getNewCode()
                        }
                    })
            }
        }
    }

    fun getPhoneNumber(): String {
        return mPhoneNumber
    }

    // Helper funcs
    private fun showNoInternet() {
        Toast.makeText(context, "Нет интернета", Toast.LENGTH_SHORT).show()
    }

    private fun getNewCode() {
        linearNewCode.visibility = View.INVISIBLE
        linearWait.visibility = View.VISIBLE
        startWaitTimer()
    }

    private fun startWaitTimer() {
        GlobalScope.launch(Dispatchers.IO) {
            for (i in timeToCode downTo 1) {
                timerSeconds.postValue(i - 1)
                delay(1000)
            }
            timerSeconds.postValue(null)
        }
    }

    // Number functions
    private fun showEditNumber(edit: Int, number: String) {
        when (edit) {
            0 -> showNumber(ellipse1, circle1, ellipse2, circle2, number1, number)
            1 -> showNumber(ellipse2, circle2, ellipse3, circle3, number2, number)
            2 -> showNumber(ellipse3, circle3, ellipse4, circle4, number3, number)
            3 -> showNumber(ellipse4, circle4, ellipse5, circle5, number4, number)
            4 -> {
                showNumber(ellipse5, circle5, null, null, number5, number)
                if (isNetworkEnable.not()) {
                    showNoInternet()
                    return
                }
                hideKeyboard(context)
                startCheckCode()
                AuthRepository.checkCode(userCode.toInt()).observe(this, Observer { result ->
                    finishCheckCode()
                    if (result.data == null)
                        wrongCode()
                    else {
                        MainObject.mCallback?.onAuthorized(
                            result.data!!.user.uid,
                            result.data!!.needNick
                        )
                    }
                })
            }
        }
    }

    private fun wrongCode() {
//        editNumbers.text.clear()
//        currentNumber = -1
//        userCode = ""
        textWrongCode.visibility = View.VISIBLE
//        for (i in 4 downTo 0) {
//            hideEditNumber(i)
//        }
    }

    private fun hideEditNumber(edit: Int) {
        when (edit) {
            0 -> hideNumber(circle1, ellipse2, circle2, number1)
            1 -> hideNumber(circle2, ellipse3, circle3, number2)
            2 -> hideNumber(circle3, ellipse4, circle4, number3)
            3 -> hideNumber(circle4, ellipse5, circle5, number4)
            4 -> hideNumber(circle5, null, null, number5)
        }
    }

    private fun showNumber(
        ellipse: ImageView,
        circle: ImageView,
        nextEllipse: ImageView?,
        nextCircle: ImageView?,
        editNumber: TextView,
        number: String
    ) {
        textWrongCode.visibility = View.INVISIBLE
        ellipse.visibility = View.INVISIBLE
        circle.visibility = View.INVISIBLE
        nextEllipse?.visibility = View.INVISIBLE
        nextCircle?.visibility = View.VISIBLE
        editNumber.visibility = View.VISIBLE
        editNumber.text = number
        userCode += number
    }

    private fun hideNumber(
        circle: ImageView,
        prevEllipse: ImageView?,
        prevCircle: ImageView?,
        editNumber: TextView
    ) {
        textWrongCode.visibility = View.INVISIBLE
        circle.visibility = View.VISIBLE
        prevEllipse?.visibility = View.VISIBLE
        prevCircle?.visibility = View.INVISIBLE
        editNumber.visibility = View.INVISIBLE
        editNumber.text = "0"
        userCode = userCode.dropLast(1)
    }

    // Checking code
    private fun startCheckCode() {
        textCheckCode.visibility = View.VISIBLE
        progressSendingCode.visibility = View.VISIBLE
    }

    private fun finishCheckCode() {
        textCheckCode.visibility = View.INVISIBLE
        progressSendingCode.visibility = View.INVISIBLE
    }
}