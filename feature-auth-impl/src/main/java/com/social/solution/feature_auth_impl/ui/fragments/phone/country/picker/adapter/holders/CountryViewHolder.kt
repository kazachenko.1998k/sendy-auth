package com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.CountryPickerFragment
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.models.CountryItemModel
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.models.CountryModel
import kotlinx.android.synthetic.main.item_country.view.*

class CountryViewHolder(itemView: View): BaseCountryViewHolder(itemView) {
    override fun populate(countryItemModel: CountryItemModel, listener: View.OnClickListener) {
        val countryModel = countryItemModel.countryModel ?: return
        itemView.imageFlag.setImageResource(countryModel.fragId)
        itemView.textCountryName.text = countryModel.countryName
        itemView.setOnClickListener {
            CountryPickerFragment.lastCountryName = countryModel.countryNameCode
            listener.onClick(it)
        }
    }
}