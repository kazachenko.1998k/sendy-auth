package com.social.solution.feature_auth_impl.utils

import android.content.Context
import android.content.SharedPreferences
import android.view.View
import android.widget.Button
import com.social.solution.feature_auth_impl.R

fun View.initTermsOfUse(termsView: View,
                        buttonAccept: Button) {
    val setAcceptVisibility =  { accepted: Boolean ->
        termsView.visibility = if (accepted) View.GONE else View.VISIBLE
        this.visibility = if (accepted) View.VISIBLE else View.GONE
    }
    setAcceptVisibility(context.isTermsOfUseAccepted())
    buttonAccept.setOnClickListener {
        context.acceptTermsOfUse()
        setAcceptVisibility(true)
        buttonAccept.isEnabled = false
    }
}

private fun Context.acceptTermsOfUse()
        = getSharedPreferences().edit().putBoolean(resources.getString(R.string.terms_accepted_sp), true).apply()

private fun Context.isTermsOfUseAccepted(): Boolean
        = getSharedPreferences().getBoolean(resources.getString(R.string.terms_accepted_sp), false)

private fun Context.getSharedPreferences(): SharedPreferences
        = getSharedPreferences(resources.getString(R.string.terms_shared_preferences), Context.MODE_PRIVATE)