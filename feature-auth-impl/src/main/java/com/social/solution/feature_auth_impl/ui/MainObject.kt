package com.social.solution.feature_auth_impl.ui

import androidx.lifecycle.LiveData
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_auth_api.FeatureAuthorizationCallback


object MainObject {
    
    var mCallback: FeatureAuthorizationCallback? = null
    var mBackendApi: FeatureBackendApi? = null
    var mBackendApiLiveData: LiveData<FeatureBackendApi?>? = null
    var mNetworkState: LiveData<Boolean>? = null
}