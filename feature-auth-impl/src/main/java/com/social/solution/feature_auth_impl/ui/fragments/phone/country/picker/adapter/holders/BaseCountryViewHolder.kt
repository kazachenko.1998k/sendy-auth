package com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.models.CountryItemModel

abstract class BaseCountryViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    abstract fun populate(countryItemModel: CountryItemModel, listener: View.OnClickListener)
}