package com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.models

data class CountryModel(val fragId: Int, val countryName: String, val countryNameCode: String, val countryCode: Int)