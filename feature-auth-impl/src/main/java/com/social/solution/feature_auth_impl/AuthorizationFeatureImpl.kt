package com.social.solution.feature_auth_impl

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_auth_api.AuthorizationFeatureApi
import com.social.solution.feature_auth_api.AuthorizationStarter
import com.social.solution.feature_auth_impl.ui.MainObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AuthorizationFeatureImpl(
    private val starter: AuthorizationStarter
) : AuthorizationFeatureApi {

    override fun authorizationStarter(): AuthorizationStarter = starter

    @SuppressLint("CheckResult")
    override fun isAuthorized(): MutableLiveData<Boolean> {
        val result = MutableLiveData<Boolean>()

//        api.authApi().isAuthorized()
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribeOn(Schedulers.io())
//            .subscribe({
//                if (it == null || !it.isResponseSuccessful || it.data == null) {
//                    result.value = false
//                } else {
//                    result.value = it.data!!.isAllowed
//                }
//            }, {
//                result.value = false
//                it.printStackTrace()
//            })
        return result
    }
}