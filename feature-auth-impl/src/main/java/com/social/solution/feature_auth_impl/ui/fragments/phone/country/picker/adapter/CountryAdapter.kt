package com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter

import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.social.solution.feature_auth_impl.R
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.holders.BaseCountryViewHolder
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.holders.CountryViewHolder
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.holders.HeaderLetterViewHolder
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.holders.SearchViewHolder
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.models.CountryItemModel
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.adapter.models.HolderTypeModel
import com.social.solution.feature_auth_impl.utils.ListAdapter

open class CountryAdapter: ListAdapter<CountryItemModel, BaseCountryViewHolder>() {

    var mSearchTextWatcher: TextWatcher? = null
    var listener: View.OnClickListener? = null

    override fun getItemViewType(position: Int): Int {
        if (position==0)
            return HolderTypeModel.SEARCH.value

        return if(data()[position].countryModel == null) {
            HolderTypeModel.HEADER.value
        } else {
            HolderTypeModel.COUNTRY.value
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseCountryViewHolder {
        return when(viewType) {
            HolderTypeModel.HEADER.value -> HeaderLetterViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_header_letter, parent, false))
            HolderTypeModel.SEARCH.value -> SearchViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_search, parent, false))
            else -> CountryViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_country, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseCountryViewHolder, position: Int) {
        if (position == 0)
            mSearchTextWatcher?.let { (holder as SearchViewHolder).populate(it) }
        else
            listener?.let { holder.populate(data()[position], it) }
    }
}