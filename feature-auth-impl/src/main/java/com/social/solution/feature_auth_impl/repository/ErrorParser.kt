package com.social.solution.feature_auth_impl.repository

import android.content.res.Resources
import com.morozov.core_backend_api.errors.Error
import com.social.solution.feature_auth_impl.R

/**
 * @return Error message
 * */
fun Error.parseSignAnonym(resources: Resources): String {
    return when(code) {
        407 -> resources.getString(R.string.too_fast_try_later)
        430 -> resources.getString(R.string.user_already_logined)
        else -> msg
    }
}

/**
 * @return Error message
 * */
fun Error.parseSignPhone(resources: Resources): String {
    return when(code) {
        430 -> resources.getString(R.string.user_already_logined)
        else -> msg
    }
}