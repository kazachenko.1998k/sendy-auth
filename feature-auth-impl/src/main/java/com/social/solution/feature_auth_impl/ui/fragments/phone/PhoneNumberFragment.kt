package com.social.solution.feature_auth_impl.ui.fragments.phone

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.morozov.core_backend_api.SignData
import com.morozov.core_backend_api.auth.AuthApi
import com.social.solution.feature_auth_impl.R
import com.social.solution.feature_auth_impl.repository.parseSignAnonym
import com.social.solution.feature_auth_impl.repository.parseSignPhone
import com.social.solution.feature_auth_impl.repository.setSignAnonymButton
import com.social.solution.feature_auth_impl.repository.setSignPhoneButton
import com.social.solution.feature_auth_impl.ui.MainObject
import com.social.solution.feature_auth_impl.ui.fragments.phone.country.picker.CountryPickerFragment
import com.social.solution.feature_auth_impl.ui.fragments.sms.SmsCodeFragment
import com.social.solution.feature_auth_impl.utils.hideKeyboard
import com.social.solution.feature_auth_impl.utils.highlightPartAccent
import com.social.solution.feature_auth_impl.utils.initTermsOfUse
import kotlinx.android.synthetic.main.fragment_phone_number.*
import kotlinx.android.synthetic.main.view_terms_of_use.*
import java.util.*

class PhoneNumberFragment : Fragment() {
    companion object {
        const val TAG = "PhoneNumberFragment_TAG"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_phone_number, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        MainObject.mBackendApiLiveData?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            Log.i("Jeka", "Set feature back end api: $it")
            if (it != null) {
                initSignPhone(view, SignData.sign, it.authApi())
                initSignAnonym(SignData.sign, it.authApi())
                MainObject.mBackendApi = it
            }
        })

        ccp.registerCarrierNumberEditText(editText_carrierNumber)
        CountryPickerFragment.lastCountryName?.let { ccp.setCountryForNameCode(it) }

        MainObject.mBackendApi?.authApi()?.let { initSignPhone(view, SignData.sign, it) }
        MainObject.mBackendApi?.authApi()?.let { initSignAnonym(SignData.sign, it) }

        ccp.setPhoneNumberValidityChangeListener {
            isNumberReady(it)
        }
        ccp.overrideClickListener {
            hideKeyboard(context)
            CountryPickerFragment.ccp = ccp
            Navigation.findNavController(view)
                .navigate(R.id.action_phoneNumberFragment_to_countryPickerFragment)
        }

        MainObject.mNetworkState?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it) {
                isNumberReady(ccp.isValidFullNumber)
            } else {
                isNumberReady(ccp.isValidFullNumber)
            }
        })

        constraintAuth.initTermsOfUse(constraintTermsOfUse, buttonAccept)

        textTermsAuth.highlightPartAccent("Условиями оферты") {}
        textTerms.highlightPartAccent("Условиями оферты") {}
    }

    @SuppressLint("HardwareIds")
    private fun initSignPhone(view: View, sign: String, authApi: AuthApi) {
        buttonSendCode.setSignPhoneButton(
            activity,
            viewLifecycleOwner,
            sign,
            authApi,
            this::getPhoneNumberClick
        )
        { response ->
            progressSendingPhone.visibility = View.INVISIBLE
            if (response.data == null || response.result.not()) {
                if (response.error != null)
                    Toast.makeText(
                        context,
                        response.error!!.parseSignPhone(resources),
                        Toast.LENGTH_SHORT
                    ).show()
                else
                    Toast.makeText(
                        context,
                        resources.getString(R.string.error_response),
                        Toast.LENGTH_SHORT
                    ).show()
            } else {
                Log.d("WebSocket", response.rid.toString() + " " + response.data.toString())
                SmsCodeFragment.timeToCode =
                    ((response.data!!.nextCodeTime * 1000 - Calendar.getInstance().timeInMillis) / 1000 + 1).toInt()
                val bundle = Bundle()
                bundle.putString("phoneNumber", ccp.fullNumber)
                Navigation.findNavController(view)
                    .navigate(R.id.action_phoneNumberFragment_to_smsCodeFragment, bundle)
            }
        }
    }

    @SuppressLint("HardwareIds")
    private fun initSignAnonym(sign: String, authApi: AuthApi) {
        textSignAnonym.setSignAnonymButton(activity, viewLifecycleOwner, sign, authApi) { event ->
            if (event.result.not()) {
                if (event.error != null)
                    Toast.makeText(
                        context,
                        event.error!!.parseSignAnonym(resources),
                        Toast.LENGTH_SHORT
                    ).show()
                else
                    Toast.makeText(
                        context,
                        resources.getString(R.string.error_response),
                        Toast.LENGTH_SHORT
                    ).show()
            } else {
                MainObject.mCallback?.onSignedAnonym(event.data!!.user.uid.toLong())
            }
        }
    }

    private fun getPhoneNumberClick(): String {
        progressSendingPhone.visibility = View.VISIBLE
        return ccp.fullNumber
    }

    private fun isNumberReady(bool: Boolean) {
        if (bool && MainObject.mNetworkState?.value == true) {
            buttonSendCode.isEnabled = true
            buttonSendCode.backgroundTintList =
                context?.resources?.getColorStateList(R.color.colorAccent)
        } else {
            buttonSendCode.isEnabled = false
            buttonSendCode.backgroundTintList =
                context?.resources?.getColorStateList(R.color.auth_light_grey)
        }
    }
}